 # Password Generator 

This is small project which generates random password for the user.

# Tech/Framework

This project developed in python3 in Visual Studio Code IDE.

# Features

- This small project generates random password by taking length of the password, number of letters and digits. Random generated password contains atleast one uppercase and lowercase letter, one digit and one special character.

- After generating random password that password will be sent to user email address which is provided by the user.
